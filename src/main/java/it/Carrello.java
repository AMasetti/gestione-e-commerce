package it;

import java.util.HashMap;
import java.util.Map;

public class Carrello {
    protected HashMap<Prodotto, Integer> prodotti;
    protected Promozione promozione;

    public Carrello(HashMap<Prodotto, Integer> prodotti, Promozione promozione) {
        prodotti = new HashMap<>();
        this.prodotti = prodotti;
        this.promozione = promozione;
    }
    public Carrello(HashMap<Prodotto, Integer> prodotti) {
        prodotti = new HashMap<>();
        this.prodotti = prodotti;
    }

    public Carrello() {
        prodotti = new HashMap<>();
    }

    public HashMap<Prodotto, Integer> getProdotti() {
        return prodotti;
    }

    public void setProdotti(HashMap<Prodotto, Integer> prodotti) {
        this.prodotti = prodotti;
    }

    public Promozione getPromozione() {
        return promozione;
    }

    public void setPromozione(Promozione promozione) {
        this.promozione = promozione;
    }

    public void aggiungiProdotto(Prodotto prodotto, int quantita){
        if (quantita <= prodotto.getQuantitaInventario()) {
            this.prodotti.put(prodotto, quantita);
        }
        else System.out.println("quantità non disponibile");
    }
    public void rimuoviProdotto(Prodotto prodotto) {
        if (this.prodotti.containsKey(prodotto)) {
            this.prodotti.remove(prodotto);
        }
        else System.out.println("prodotto non trovato");
    }


    public double calcolaTotale(){
        double totale = 0;
        if (this.prodotti.isEmpty()) return 0;
        else{
            for (Map.Entry<Prodotto, Integer> i : this.prodotti.entrySet()) {
                    totale += i.getValue() * i.getKey().prezzo;
            }
            if (this.promozione == null) {
                return totale;
            }
            else {
                return totale - (totale * promozione.percentualeSconto);
            }
        }

    }

}
