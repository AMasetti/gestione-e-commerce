package it;
import java.util.HashMap;
import java.util.Map;

public class GestioneInventario {
    private Map<Prodotto, Integer> prodotti;


    public GestioneInventario(Map<Prodotto, Integer> prodotti) {
        this.prodotti = new HashMap<Prodotto, Integer>(prodotti);
    }
    public GestioneInventario() {

        this.prodotti = new HashMap<>();
    }

    public Map<Prodotto, Integer> getProdotti() {
        return prodotti;
    }

    public void setProdotti(Map<Prodotto, Integer> prodotti) {
        this.prodotti = prodotti;
    }

    public boolean controllaDisponibilitaProdotto(Prodotto prodotto) {

        return this.prodotti.containsKey(prodotto);
    }

    public void aggiornaProdotto(Prodotto prodotto, int quantita) {
        this.prodotti.put(prodotto, quantita);
    }

    public int controlloQuantitaProdotto(Prodotto prodotto) {
        if (this.controllaDisponibilitaProdotto(prodotto)){
        return this.prodotti.get(prodotto);
    }
        else {
        return 0;
        }
    }
}
