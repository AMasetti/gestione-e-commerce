package it;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Ordine {
    protected String idOrdine;
    protected HashMap<Prodotto, Integer> prodotti;

    public Stato stato;
    public enum Stato {
        IN_ATTESA,
        SPEDITO,
        CONSEGNATO
    }

    protected double totaleOrdine;


    public Ordine(HashMap<Prodotto, Integer> prodotti, Stato stato) {
        prodotti = new HashMap<>();
        this.prodotti = prodotti;
        this.stato = stato;
    }

    public Ordine(){
        prodotti = new HashMap<>();
    }

    public Ordine(Stato stato){
        prodotti = new HashMap<>();
        this.stato = stato;
    }

    public Stato getStato() {
        return stato;
    }

    public void setStato(Stato stato) {
        this.stato = stato;
    }

    public String getIdOrdine() {
        return idOrdine;
    }

    public void setIdOrdine(String idOrdine) {
        this.idOrdine = idOrdine;
    }

    public double getTotaleOrdine() {
        return totaleOrdine;
    }

    public void setTotaleOrdine(double totaleOrdine) {
        this.totaleOrdine = totaleOrdine;
    }

    public HashMap<Prodotto, Integer> getProdotti() {
        return prodotti;
    }

    public void setProdotti(HashMap<Prodotto, Integer> prodotti) {
        this.prodotti = prodotti;
    }

    public void aggiungiProdotto(Prodotto prodotto, int quantita){
        if (quantita <= prodotto.getQuantitaInventario()) {
            this.prodotti.put(prodotto, quantita);
        }
        else System.out.println("quantità non disponibile");
    }
     public void rimuoviProdotto(Prodotto prodotto) {
         if (this.prodotti.containsKey(prodotto)) {
             this.prodotti.remove(prodotto);
         }
         else System.out.println("prodotto non trovato");
     }


     public double calcolaTotale(){
         double totale = 0;
        if (this.prodotti.isEmpty()) return 0;
        else{
            for (Map.Entry<Prodotto, Integer> i : this.prodotti.entrySet()) {
                totale += i.getValue() * i.getKey().prezzo;
            }
            this.totaleOrdine = totale;
            return totale;
        }

     }

     public void aggiornaStato(Stato stato){
        this.stato = stato;
     }



}
