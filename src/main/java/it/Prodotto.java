package it;

public class Prodotto {
    protected String id;
    protected String nome;
    protected double prezzo;
    protected int quantitaInventario;

    public Prodotto(String id, String nome, double prezzo, int quantitaInventario) {
        this.id = id;
        this.nome = nome;
        this.prezzo = prezzo;
        this.quantitaInventario = quantitaInventario;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }

    public int getQuantitaInventario() {
        return quantitaInventario;
    }

    public void setQuantitàInventario(int quantitaInventario) {
        this.quantitaInventario = quantitaInventario;
    }
}
