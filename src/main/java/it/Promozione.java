package it;

public class Promozione {
    protected String cocePromozione;
    protected String descrizione;
    protected double percentualeSconto;

    public Promozione(String cocePromozione, String descrizione, double percentualeSconto) {
        this.cocePromozione = cocePromozione;
        this.descrizione = descrizione;
        this.percentualeSconto = percentualeSconto;
    }

    public void applicaSconto(Ordine ordine){
        ordine.setTotaleOrdine(ordine.getTotaleOrdine()*(1-this.percentualeSconto));
    }


}
