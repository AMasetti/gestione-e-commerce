package it;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class CarrelloTest {

    private GestioneInventario gestioneInventario;
    private Carrello carrello;
    private Prodotto prodotto1;

    private Promozione promozione;

    @BeforeEach
    void setUp() {
        gestioneInventario = new GestioneInventario();
        promozione = new Promozione("1", "sconto imperdibile", 0.20);
        carrello = new Carrello((HashMap<Prodotto, Integer>)gestioneInventario.getProdotti(),promozione);
        prodotto1 = new Prodotto("1", "Maglietta", 20, 15);
        carrello.aggiungiProdotto(prodotto1, 1);
    }

    @Test
    void aggiungiProdotto() {
        assertEquals(1, carrello.getProdotti().size());
        assertTrue(carrello.getProdotti().containsKey(prodotto1));
    }

    @Test
    void rimuoviProdotto() {
        carrello.rimuoviProdotto(prodotto1);
        assertEquals(0, carrello.getProdotti().size());
    }

    @Test
    void calcolaTotale() {
        assertEquals(16, carrello.calcolaTotale());
    }
}