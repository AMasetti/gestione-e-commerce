package it;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class GestioneInventarioTest {

    private GestioneInventario gestioneInventario;
    private Prodotto prodotto1;
    private Prodotto prodotto2;

    @BeforeEach
    void setUp() {
        gestioneInventario = new GestioneInventario();
        prodotto1 = new Prodotto("1", "Maglietta", 20, 15);
        prodotto2 = new Prodotto("2", "Scarpe", 60, 20);
        gestioneInventario.aggiornaProdotto(prodotto1, 15);
    }

    @Test
    void controllaDisponibilitaProdotto() {
        assertTrue(gestioneInventario.controllaDisponibilitaProdotto(prodotto1));
    }

    @Test
    void aggiornaProdotto() {

        assertEquals(1, gestioneInventario.getProdotti().size());
        gestioneInventario.aggiornaProdotto(prodotto1, 25);
        assertEquals(1, gestioneInventario.getProdotti().size());

    }

    @Test
    void controlloQuantitaProdotto() {
        assertEquals(15, gestioneInventario.controlloQuantitaProdotto(prodotto1));
        assertEquals(0, gestioneInventario.controlloQuantitaProdotto(prodotto2));
        gestioneInventario.aggiornaProdotto(prodotto1, 25);
        assertEquals(25, gestioneInventario.controlloQuantitaProdotto(prodotto1));

    }
}