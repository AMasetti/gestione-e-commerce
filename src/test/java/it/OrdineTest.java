package it;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class OrdineTest {

    private Ordine ordine;

    private Prodotto prodotto1;
    private GestioneInventario gestioneInventario;

    @BeforeEach
    void setUp() {
        gestioneInventario = new GestioneInventario();
        ordine = new Ordine((HashMap<Prodotto, Integer>)gestioneInventario.getProdotti(), Ordine.Stato.IN_ATTESA);
        prodotto1 = new Prodotto("1", "Maglietta", 23, 15);

    }

    @Test
    void aggiungiProdotto() {
        ordine.aggiungiProdotto(prodotto1, 1);
        assertEquals(1, ordine.getProdotti().size());
        assertTrue(ordine.getProdotti().containsKey(prodotto1));
    }

    @Test
    void rimuoviProdotto() {
        ordine.aggiungiProdotto(prodotto1, 1);
        ordine.rimuoviProdotto(prodotto1);
        assertEquals(0, ordine.getProdotti().size());
    }

    @Test
    void aggiornaStato() {
        ordine.aggiornaStato(Ordine.Stato.CONSEGNATO);
        assertEquals(Ordine.Stato.CONSEGNATO, ordine.getStato());
    }
}