package it;
import it.Carrello;
import it.GestioneInventario;
import it.Ordine;
import it.Prodotto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static it.Ordine.Stato.IN_ATTESA;
import static org.junit.jupiter.api.Assertions.*;

class ProdottoTest {

    private Prodotto prodotto1;
    private GestioneInventario gestioneInventario;
    private Carrello carrello;

    @BeforeEach
    void setUp() {

        prodotto1 = new Prodotto("1", "maglietta", 23, 25);
        gestioneInventario = new GestioneInventario();
        gestioneInventario.aggiornaProdotto(prodotto1, prodotto1.getQuantitaInventario());
        carrello = new Carrello((HashMap<Prodotto, Integer>) gestioneInventario.getProdotti());
        //Ordine ordine = new Ordine(carrello.getProdotti(), IN_ATTESA);
    }

    @Test
    void testControlloQuantita() {
        carrello.aggiungiProdotto(prodotto1, 3);
        assertEquals(prodotto1.getQuantitaInventario(), gestioneInventario.controlloQuantitaProdotto(prodotto1));
    }
}