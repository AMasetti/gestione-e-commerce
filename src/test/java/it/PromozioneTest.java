package it;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class PromozioneTest {
    private Promozione promozione;
    private Ordine ordine;
    private GestioneInventario gestioneInventario;
    private Prodotto prodotto1;

    @BeforeEach
    void setUp() {

        promozione = new Promozione("1", "sconto imperdibile", 0.20);
        gestioneInventario = new GestioneInventario();
        ordine = new Ordine((HashMap<Prodotto, Integer>) gestioneInventario.getProdotti(), Ordine.Stato.IN_ATTESA);
        prodotto1 = new Prodotto("1", "Maglietta", 20, 15);
        ordine.aggiungiProdotto(prodotto1, 1);
    }

    @Test
    void testApplicaSconto() {
        ordine.aggiungiProdotto(prodotto1, 1);
        ordine.calcolaTotale();
        promozione.applicaSconto(ordine);
        assertEquals(16, ordine.getTotaleOrdine());
    }
}